const db = require("../models");

// create main model
const Review = db.reviews;

//functions
//Add review
const addReview = async (req, res) => {
  try {
    let data = {
      rating: req.body.rating,
      description: req.body.description,
      product_id: req.body.product_id,
    };
    const review = await Review.create(data);
    return res.status(201).json(review);
  } catch (error) {
    console.log(error);
    return res.status(500).json("Something went wrong");
  }
};

// Get all reviews
const getAllReviews = async (req, res) => {
  try {
    const reviews = await Review.findAll({});
    return res.status(200).json(reviews);
  } catch (error) {
    console.log(error);
    return res.status(500).json("Something went wrong");
  }
};

module.exports = {
  addReview,
  getAllReviews,
};
