const productController = require("../controllers/productController");

const router = require("express").Router();

router.post("/addproduct", productController.addProduct);
router.get("/allproducts", productController.getAllProducts);
router.get("/published", productController.getPublishedProduct);

router.get("/:id", productController.getSingleProduct);
router.put("/:id", productController.updateProduct);
router.delete("/:id", productController.deleteProduct);

router.get("/get-product-review", productController.getProductReviews);

module.exports = router;
