const db = require("../models");

// create main model
const Product = db.products;
const Review = db.reviews;

// main work

// 1. Create product
const addProduct = async (req, res) => {
  try {
    let info = {
      title: req.body.title,
      price: req.body.price,
      description: req.body.description,
      published: req.body.published ? req.body.published : false,
    };

    const product = await Product.create(info);
    res.status(201).send(product);
  } catch (error) {
    console.log(error);
    return res.status(500).json({ msg: "Something went wrong" });
  }
};

// Get all products
const getAllProducts = async (req, res) => {
  try {
    let products = await Product.findAll({
      attributes: ["title", "price"],
    });
    return res.status(200).json(products);
  } catch (error) {
    console.log(error);
    return res.status(500).json({ msg: "Something went wrong" });
  }
};

// Get single products
const getSingleProduct = async (req, res) => {
  let id = req.params.id;
  try {
    let product = await Product.findOne({ where: { id: id } });
    return res.status(200).json(product);
  } catch (error) {
    console.log(error);
    return res.status(500).json({ msg: "Something went wrong" });
  }
};

// update product by id
const updateProduct = async (req, res) => {
  try {
    let id = req.params.id;
    const product = await Product.update(req.body, { where: { id: id } });
    return res.status(200).json(product);
  } catch (error) {
    console.log(error);
    return res.status(500).json({ msg: "Something went wrong" });
  }
};

// delete product by id
const deleteProduct = async (req, res) => {
  let id = req.params.id;
  try {
    const product = await Product.destroy({ where: { id: id } });
    return res.status(200).json(`${id} is deleted`);
  } catch (error) {
    console.log(error);
    return res.status(500).json({ msg: "Something went wrong" });
  }
};

// get published product
const getPublishedProduct = async (req, res) => {
  try {
    const products = await Product.findAll({ where: { published: true } });
    return res.status(200).json(products);
  } catch (error) {
    console.log(error);
    return res.status(500).json({ msg: "Something went wrong" });
  }
};

// Connect one to many relation
const getProductReviews = async (req, res) => {
  try {
    const data = await Product.findAll({
      include: [
        {
          model: Review,
          as: "review",
        },
      ],
      where: { id: 2 },
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ msg: "Something went wrong" });
  }
};
module.exports = {
  getPublishedProduct,
  addProduct,
  getAllProducts,
  getSingleProduct,
  updateProduct,
  deleteProduct,
  getProductReviews,
};
