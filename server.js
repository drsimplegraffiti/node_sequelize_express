const express = require("express");
const cors = require("cors");

const app = express();

const port = process.env.PORT || 8080;
const corsOption = {
  origin: "https://localhost:8081",
};

// mount router
const router = require("./router/productRouter");
const reviewRouter = require("./router/reviewRouter");

//middleware
app.use(cors(corsOption));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Health Check
app.get("/", (req, res) => {
  res.json({ msg: "landing page" });
});

app.use("/api/products", router);
app.use("/api/reviews", reviewRouter);

// Server listening
app.listen(port, () => {
  console.log(`APP RUNNING ON PORT ${port}`);
});
